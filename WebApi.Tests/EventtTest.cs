using System;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using Domain;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Repositories;
using WebApi.Tests.Support;

namespace WebApi.Tests
{
    [TestFixture]
    public class EventtTest
    {
        private HttpClient client;
        private TestWebApplicationFactory<Startup> factory;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            this.factory = new TestWebApplicationFactory<Startup>();
            this.client = factory.CreateClient();
        }

        [SetUp]
        public void Setup()
        {
            using (var testScope = factory.Services.CreateScope())
            {
                var repo = (EventtRepository)testScope.ServiceProvider.GetService<IEventtRepository>();
                repo.Reset();
            }
        }

        [Test]
        public void EndUserCanCreateAnEvent()
        {
            var response = CreateEvent();

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };
            var jsonResult = JsonSerializer.Deserialize<Eventt>(response.Content.ReadAsStringAsync().Result, options);
            Assert.That(jsonResult.Id, Is.GreaterThan(0));
        }

        [Test]
        public void UsersCannotScheduleEventsThatOverlap()
        {
            var response = CreateEvent();
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
            response = CreateEvent();
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Conflict));
        }

        private HttpResponseMessage CreateEvent()
        {
            var eventtToCreate = new
            {
                user = "john",
                title = "Gym Session",
                startDateTime = DateTime.Now.AddHours(1)
            };

            var httpContent = new StringContent(JsonSerializer.Serialize(eventtToCreate));
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

            var response = client.PostAsync("/events", httpContent).Result;
            return response;
        }

    }


}
