using System.Dynamic;
using System.Net;
using System.Text.Json;
using NUnit.Framework;
using WebApi.Helpers;
using WebApi.Tests.Support;

namespace WebApi.Tests
{
    public class Tests
    {
        [Test]
        public void ReturnsStatusOkWhenRunningHealty()
        {
            var factory = new TestWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var response = client.GetAsync("/health").Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public void ReturnsAppVersion()
        {
            var factory = new TestWebApplicationFactory<Startup>();
            var client = factory.CreateClient();

            var response = client.GetAsync("/health").Result;

            dynamic jsonResult = JsonSerializer.Deserialize<ExpandoObject>(response.Content.ReadAsStringAsync().Result);
            var version = typeof(WebApi.Startup).Assembly.GetName().Version;
            if (version != null)
            {
                var expectedVersion = version.ToString();
                Assert.That(jsonResult.version.ToString(), Is.SupersetOf(expectedVersion));
            }

        }
    }
}
