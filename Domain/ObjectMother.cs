using System;

namespace Domain
{
    public class ObjectMother
    {
        public static Eventt SampleEventt()
        {
            return new Eventt(user: "john",
                title: "gym session",
                startDatetime: DateTime.Now.AddHours(1));
        }

        public static Eventt SampleEventtInXHours(int hours)
        {
            return new Eventt(user: "john",
                title: "gym session",
                startDatetime: DateTime.Now.AddHours(hours));
        }
    }
}
