using System;

namespace Domain
{
    public class Eventt
    {
        public Eventt()
        {

        }

        public Eventt(string user, string title, DateTime startDatetime)
        {
            this.User = user;
            this.Title = title;
            this.StartDateTime = startDatetime;
        }

        public int Id { get; set; }

        public DateTime StartDateTime { get; set; }

        public string Title { get; set; }

        public string User { get; set; }

        public bool OverlapsWith(Eventt otherEvent)
        {
            return this.StartDateTime.AddHours(1) >= otherEvent.StartDateTime;
        }
    }
}
