﻿using System;

namespace Domain
{
    public class CalendarSystem
    {
        private readonly IEventtRepository eventtRepository;

        public CalendarSystem(IEventtRepository eventtRepository)
        {
            this.eventtRepository = eventtRepository;
        }

        public Eventt CreateEvent(string user, string title, DateTime startDatetime)
        {
            var newEventt = new Eventt(user, title, startDatetime);
            var userEvents = this.eventtRepository.FindByUser(user);
            foreach (var eventt in userEvents)
            {
                if (eventt.OverlapsWith(newEventt))
                {
                    throw new OverlappedEventsException();
                }
            }
            this.eventtRepository.Save(newEventt);
            return newEventt;
        }
    }
}
