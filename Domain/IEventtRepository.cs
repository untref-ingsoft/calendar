using System.Collections.Generic;

namespace Domain
{
    public interface IEventtRepository
    {
        void Save(Eventt eventt);
        IEnumerable<Eventt> FindByUser(string user);
    }
}
