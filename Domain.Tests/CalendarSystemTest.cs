using System;
using System.Collections.Generic;
using System.Threading;
using Moq;
using NUnit.Framework;

namespace Domain.Tests
{
    public class CalendarSystemTest
    {
        [Test]
        public void CreateEventSavesTheCreateObject()
        {
            var eventtRepository = new Mock<IEventtRepository>();
            var calendar = new CalendarSystem(eventtRepository.Object);

            var eventt = calendar.CreateEvent("john", "gym session", DateTime.Now.AddHours(1));

            eventtRepository.Verify(x=> x.Save(It.IsAny<Eventt>()), Times.Once);
            Assert.IsNotNull(eventt);
        }

        [Test]
        public void CannotCreateEventsThatOverlap()
        {
            var eventtRepository = new Mock<IEventtRepository>();
            eventtRepository.SetupSequence(x => x.FindByUser("john"))
                .Returns(new List<Eventt>())
                .Returns(new List<Eventt>() { new Eventt { User="john", Title="gym session", StartDateTime = DateTime.Now.AddHours(1) } });

            var calendar = new CalendarSystem(eventtRepository.Object);

            var eventt = calendar.CreateEvent("john", "gym session", DateTime.Now.AddHours(1));
            Assert.Throws<OverlappedEventsException>(() =>calendar.CreateEvent("john", "gym session", DateTime.Now.AddHours(1).AddMinutes(30)));
        }
    }
}
