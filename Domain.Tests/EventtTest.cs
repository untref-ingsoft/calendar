using NUnit.Framework;

namespace Domain.Tests
{
    [TestFixture]
    public class EventtTest
    {
        [Test, Category("unit")]
        public void EventsOverlapWhenStarAtTheSameTime()
        {
            var event1 = ObjectMother.SampleEventt();
            var event2 = ObjectMother.SampleEventt();

            Assert.That(event1.OverlapsWith(event2));
        }

        [Test, Category("unit")]
        public void EventsNotOverlapWhenStarWithMoireThanAnHourDiff()
        {
            var event1 = ObjectMother.SampleEventt();
            var event2 = ObjectMother.SampleEventtInXHours(3);

            Assert.That(event1.OverlapsWith(event2), Is.False);
        }
    }
}
