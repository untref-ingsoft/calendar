using Domain;
using NUnit.Framework;
using System;
using System.Linq;

namespace Repositories.Tests
{
    public class EventRepositoryTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void EventtRepositoryReturnsValidIdOnSave()
        {
            var eventRepository = new EventtRepository();

            var eventt = ObjectMother.SampleEventt();

            eventRepository.Save(eventt);

            Assert.That(eventt.Id, Is.GreaterThan(0));
        }

        [Test]
        public void EventtRepositoryReturnsUniqueIdsOnSave()
        {
            var eventRepository = new EventtRepository();

            var eventtGym = ObjectMother.SampleEventt();

            var eventtDinner = ObjectMother.SampleEventt();

            eventRepository.Save(eventtGym);
            eventRepository.Save(eventtDinner);

            Assert.That(eventtGym.Id, Is.Not.EqualTo(eventtDinner.Id));
        }

        [Test]
        public void EventRepositoryFindsEventsBasedOnUsersName()
        {
            var eventRepository = new EventtRepository();

            var eventtGym = ObjectMother.SampleEventt();
            var eventtDinner = ObjectMother.SampleEventt();

            eventRepository.Save(eventtGym);
            eventRepository.Save(eventtDinner);

            Assert.That(eventRepository.FindByUser("john").ToList().Count, Is.EqualTo(2));
            Assert.That(eventRepository.FindByUser("john").Where(u => u.User == "john").Count(), Is.EqualTo(2));
        }

        [Test]
        public void EventRepositoryFindsNoEventsWhereUserNameHasNone()
        {
            var eventRepository = new EventtRepository();

            var eventtGym = ObjectMother.SampleEventt();
            var eventtDinner = ObjectMother.SampleEventt();

            eventRepository.Save(eventtGym);
            eventRepository.Save(eventtDinner);

            Assert.That(eventRepository.FindByUser("susan").ToList().Count, Is.EqualTo(0));
            Assert.That(eventRepository.FindByUser("susan").Where(u => u.User == "susan").Count(), Is.EqualTo(0));
        }
    }
}
