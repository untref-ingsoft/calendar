using System.Net;
using Domain;
using Microsoft.AspNetCore.Mvc;
using WebApi.Requests;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly CalendarSystem calendar;

        public EventsController(CalendarSystem calendar)
        {
            this.calendar = calendar;
        }

        [HttpPost]
        public ActionResult Create([FromBody] EventCreationRequest request)
        {
            try
            {
                var eventt = this.calendar.CreateEvent(request.User, request.Title, request.StartDateTime);

                return new ObjectResult(eventt)
                {
                    StatusCode = (int)HttpStatusCode.Created
                };
            }
            catch(OverlappedEventsException)
            {
                return new ConflictResult();
            }
        }
    }
}
