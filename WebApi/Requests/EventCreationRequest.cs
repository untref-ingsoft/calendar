using System;

namespace WebApi.Requests
{
    public class EventCreationRequest
    {
        public EventCreationRequest()
        {

        }

        public string User { get; set; }
        public string Title { get; set; }
        public DateTime StartDateTime { get; set; }
    }
}
