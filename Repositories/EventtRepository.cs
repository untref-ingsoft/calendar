﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repositories
{
    public class EventtRepository : IEventtRepository
    {
        private List<Eventt> eventts;

        public EventtRepository()
        {
            eventts = new List<Eventt>();
        }

        public void Save(Eventt eventt)
        {
            eventt.Id = eventts.Count + 1;

            if(!eventts.Contains(eventt))
            {
                eventts.Add(eventt);
            }
        }

        public IEnumerable<Eventt> FindByUser(string user)
        {
            return eventts.Where(e => e.User == user);
        }

        public void Reset()
        {
            eventts = new List<Eventt>();
        }
    }
}
